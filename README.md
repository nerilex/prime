# PRivacy-Intent Metadata Extension

This repository is intended to collect ideas, references and experiments relating to a proposal to include metadata regarding privacy aspects into multimedia data.


## What?

The idea is to have a picture or video ore something like that to include information about:

Is it ok to

* send this file to others
* show this file to others
* store it unencrypted
* include it in (long term) archives

And maybe also how the long the file should be kept and how to share it with others.


## Metadata Standards

* [Exif](https://en.wikipedia.org/wiki/Exif)
* [XMP](https://en.wikipedia.org/wiki/Extensible_Metadata_Platform)
* [IPTC](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model)


## ToDos:

* decide on a primary metadata standard to extend (probably XMP)
* define a set of `requests` and their specific semantics
* implement interfaces for testing
* define interface characteristics
* refine requirements
* ask library and app implementers for opinions


## FAQ

### Is this DRM?

No, this is about making it easier to others to respect your privacy intents, if you attach them to a file.


### Shouldn't we enforce this restrictions technically?

No! That would cause a lot more trouble. Just imagine someone being able to withheld proof by such means.


### Are you trying to solve social problems with technology?

 No, this tries to address the technical problems of a person willing to respect your privacy.